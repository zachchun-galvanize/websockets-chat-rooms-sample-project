import React from 'react';
import { useParams } from 'react-router-dom';


function ChatRoom() {

  // setup initial state
  const [username, setUsername] = React.useState('');
  const [usernameInput, setUsernameInput] = React.useState('');
  const [input, setInput] = React.useState('');
  const [socket, setSocket] = React.useState();
  const [messages, setMessages] = React.useState([]);
  const { id: roomId } = useParams();


  // When the roomId is updated, fetch (http request) all the previous chat messages
  React.useEffect(() => {
    const getMessages = async () => {
      const res = await fetch(`${process.env.REACT_APP_MESSAGE_API_HOST}/api/messages/${roomId}`);
      if (res.ok) {
        const results = await res.json();
        setMessages(results || []);
      }
    }
    getMessages();
  }, [roomId]);

  // When the roomId or username is updated, set up a websocket connection for this room and user
  React.useEffect(() => {
    if (!username) { return; }

    // if switching rooms, this will close the existing open socket
    if (socket) {
      socket.close()
    }

    // creates a websocket connection to the socketUrl
    // note the ws:// prefix part of the url (docker-compose environment variable)
    const socketUrl = `${process.env.REACT_APP_WS_HOST}/chatroom/${roomId}/${username}`;
    const websocket = new WebSocket(socketUrl);

    // when a new message is sent to this front-end websocke, add this message to the end of the list
    websocket.onmessage = (event) => {
      const message = JSON.parse(event.data);
      setMessages(prev => [...prev, message]);
    };

    // saves the websocket with react useState functions
    setSocket(websocket);

    // cleanup for component unmount, close the open socket
    return () => {
      websocket.close();
    }
  }, [username, roomId]);

  const handleUsernameChange = () => {
    setUsername(usernameInput);
  }
 
  const handleUsernameInputChange = (e) => {
    setUsernameInput(e.target.value);
  }

  const handleInputChange = (e) => {
    setInput(e.target.value);
  }

  const handleSendMessage = () => {
    if (!username) { return; }

    // actually sends the message (and username + room_id) to the backend via websocket
    // note this doesn't actually call `setMessages`, we just wait to see what the server says 
    // the messages are.
    socket.send(JSON.stringify({ username, room_id: roomId, input }));
    setInput('');
  }

  return (
    <div className="container my-4">
      <p>welcome to chat room {roomId}</p>
      <label>enter your username</label><input type="text" value={usernameInput} onChange={handleUsernameInputChange} />
      <button onClick={handleUsernameChange}>set username</button>
      <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'flex-start', width: '60%', margin: 'auto' }}>
        {messages.map(m => (
          <div
            key={m.id}
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: m.username === username ? 'flex-end' : 'flex-start'
            }}
          >
            {m.username !== username && (<div>{m.username}: </div>)}
            <div
              style={{
                backgroundColor: m.username === username ? 'lightgray' : 'inherit',
              }}
            >{m.input}</div>
          </div>
        ))}
        <input type="text" value={input} onChange={handleInputChange} placeholder="new message" />
        <button onClick={handleSendMessage}>send</button>
      </div>
    </div>
  );
}

export default ChatRoom;
