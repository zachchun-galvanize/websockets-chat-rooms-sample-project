import os
from fastapi import FastAPI, WebSocket, WebSocketDisconnect, Depends
from fastapi.middleware.cors import CORSMiddleware
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel
from client import Queries
from datetime import datetime
import json

app = FastAPI()

origins = [
    os.environ.get("CORS_HOST", "http://localhost:3000"),
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# nothing websocket special about this, just the structure of our data is
# centered around messages
class MessageIn(BaseModel):
    username: str
    room_id: int
    input: str
    datetime: str

class MessageOut(MessageIn):
    id: str

def sanitize_message(msg: str) -> MessageOut:
    msg["id"] = str(msg["_id"])
    del msg["_id"]
    result = MessageOut(**msg)
    return result

class MessageQueries(Queries):
    DB_NAME = "chittychat"
    COLLECTION = "messages"

    def create(self, message) -> MessageOut | None:
        res = self.collection.insert_one(jsonable_encoder(message))
        if res.inserted_id:
            return self.get_message_by_id(res.inserted_id)

    def get_message_by_id(self, id) -> MessageOut | None:
        result = self.collection.find_one({ "_id": id })
        if result:
            return sanitize_message(result)

    def get_messages_by_room_id(self, room_id) -> list[MessageOut] | None:
        results = list(self.collection.find({ "room_id": int(room_id) }).sort("datetime"))
        print(results)
        results = list(map(sanitize_message, results))
        if results:
            return results

# normal GET endpoint with http 
@app.get("/api/messages/{room_id}", response_model=list[MessageOut] | None)
async def get_messages_by_room_id(
    room_id: str,
    queries: MessageQueries = Depends(),
):
    return queries.get_messages_by_room_id(room_id)



# keys: username
# values: {websocket, room_id}
connections = {}

# required decorator from FastAPI for websocket
@app.websocket("/chatroom/{room_id}/{username}")
async def websocket_endpoint(
    room_id: str,
    username: str,
    websocket: WebSocket,
    queries: MessageQueries = Depends(),
):
    await websocket.accept()
    connections[username] = { "connection": websocket, "room_id": room_id }
    try:
        while True:
            # chilling here while there's a websocket connection open and until a message is sent
            data = await websocket.receive_text()
            parsed = json.loads(data)
            message = MessageIn(username=parsed['username'], room_id=parsed['room_id'], input=parsed['input'], datetime=datetime.now().isoformat())
            new_msg = queries.create(message)

            # find any of the users in the same room - use that user's websocket connection
            # to send this new message to them as well
            for connection in connections.values():
                if connection['room_id'] == room_id:                    
                    await connection['connection'].send_text(json.dumps(new_msg.__dict__))

    # upon disconnect, remove the active connection for that username
    except WebSocketDisconnect:
        del connections[username]
